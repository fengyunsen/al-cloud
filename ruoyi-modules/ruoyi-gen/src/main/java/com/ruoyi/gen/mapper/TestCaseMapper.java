package com.ruoyi.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.gen.domain.TestCase;

import java.util.List;

/**
 * 测试用例Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface TestCaseMapper extends BaseMapper<TestCase> {

    /**
     * 查询测试用例列表
     *
     * @param testCase 测试用例
     * @return 测试用例集合
     */
    List<TestCase> selectTestCaseList(TestCase testCase);


}
