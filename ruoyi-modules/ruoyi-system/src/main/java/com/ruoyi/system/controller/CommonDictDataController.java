package com.ruoyi.system.controller;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.common.translate.annotation.TranslateAnnotation;
import com.ruoyi.common.translate.enums.TranseTypeEnum;
import com.ruoyi.common.translate.utils.TranUtils;
import com.ruoyi.system.domain.SysDictData;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.system.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

/**
 * 数据字典信息
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/dict/data")
public class CommonDictDataController extends BaseController
{
    @Autowired
    private ISysDictDataService dictDataService;
    
    @Autowired
    private ISysDictTypeService dictTypeService;




    /**
     * 描述：  翻译实体
     * 备注：
     * 日期： 15:51 2020/7/29
     * 作者： zrd
     *
     * @param t
     * @param tClass
     * @return T
     **/
    @PreAuthorize(hasPermi = "system:dict:list")
    @GetMapping("/translateBean")
    public <T> T translateBean(T t, Class tClass) {
        return null;
    }




    /**
     * 描述：  单实体翻译工具类
     * 备注：
     * 日期： 10:08 2019/12/5
     * 作者： zrd
     *
     * @param obj
     * @return java.lang.Object
     **/
    private Object valid(Object obj, Class<?> clazz) {
        Field[] fields = ReflectUtil.getFields(clazz);
        for (Field field : fields) {
            TranslateAnnotation translateAnnotation = field.getAnnotation(TranslateAnnotation.class);//获取属性上的@Test注解
            if (translateAnnotation != null) {
                field.setAccessible(true);//设置属性可访问
                String reslut = "";
                //原始值
                try {
                    String objStr = field.get(obj).toString();
                    TranseTypeEnum transeTypeEnum = translateAnnotation.distType();
                    switch (transeTypeEnum) {
                        case DIST:
                            reslut = getDistMapByType(translateAnnotation.distCode(), objStr);
                            break;
                        case MULTIPLE_DIST:
                            reslut = getMutDistVale(translateAnnotation.distCode(), objStr);
                            break;


                        case DATE:

                            reslut = DateUtils.parseDateToStr(translateAnnotation.distCode(), (Date) field.get(obj));

                            break;
                        default:
                            reslut = "";
                            break;
                    }
                } catch (Exception e) {
                    reslut = "";
//                    logger.error("翻译异常", CommonLogEntries.createEntries(ExceptionUtil.getExceptionMessage(e)));
                }
                obj = TranUtils.taransFilds(obj, translateAnnotation.filed(), reslut);

            }
        }
        return obj;
    }



    /**
     * 描述：获取 数据字典
     * 备注：
     * 日期： 11:43 2020/1/8
     * 作者： zrd
     *
     * @param type
     * @return java.util.Map<java.lang.String, java.lang.String>
     **/
    @Override
    public String getDistMapByType(String type, String keyValue) {
        String result = keyValue;

        List<SysDictData> sysDictDataList = dictTypeService.selectDictDataByType(type);
        if (sysDictDataList != null && sysDictDataList.size() > 0) {
            for (SysDictData sysDictData : sysDictDataList) {
                if (sysDictData.getDictValue().equals(keyValue)) {
                    result = sysDictData.getDictLabel();
                    break;
                }
            }

        }


        return result;
    }

    /**
     * 描述：  获取多选下拉框的翻译
     * 备注：
     * 日期： 11:08 2020/6/3
     * 作者： zrd
     *
     * @param type
     * @param keyValue
     * @return java.lang.String
     **/
    public String getMutDistVale(String type, String keyValue) {
        String result = "";

        List<SysDictData> sysDictDataList =  dictTypeService.selectDictDataByType(type);
        if (StrUtil.isNotBlank(keyValue)) {
            String[] strArr = keyValue.split(",");
            for (String str : strArr) {
                if (sysDictDataList != null && sysDictDataList.size() > 0) {
                    for (SysDictData sysDictData : sysDictDataList) {
                        if (sysDictData.getDictValue().equals(str)) {
                            result += sysDictData.getDictLabel() + ",";
                            break;
                        }
                    }

                }
            }
        }
//如果结果为空 则不翻译
        if (StrUtil.isBlank(result)) {
            result = keyValue;
        }


        return result;
    }



}
